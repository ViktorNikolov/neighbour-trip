package com.task.tour.repositories;

import com.task.tour.models.Country;

public interface CountryRepo {

    public Country getCurrency(String countryName);
}

