package com.task.tour.repositories;

import com.task.tour.models.Country;

import java.util.List;

public interface StartingCountryRepo {

    String getFullName(String startingCountry);

    List<Country> getNeighbours(String startingCountry);

}
