package com.task.tour.repositories;

import com.task.tour.models.Country;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Repository
public class CountryRepoImpl implements CountryRepo {

    @Override
    public Country getCurrency(String countryName) {
        Country country = new Country();
        String result;

        try {
            URL url = new URL("https://restcountries.eu/rest/v2/alpha/"
                    + countryName);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);

            JSONArray jsonArray = (JSONArray) jsonObject.get("currencies");

            country.setFullName(jsonObject.getString("name"));
            country.setCurrency((jsonArray.getJSONObject(0).get("code")).toString());

            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return country;
    }

}
