package com.task.tour.repositories;

public interface OutputRepo {

    double getRespectiveCurrency(String currency, String startingCountry);

}
