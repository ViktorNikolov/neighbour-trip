package com.task.tour.repositories;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Service
public class OutputRepoImpl implements OutputRepo{

    private static final String API_KEY = "4c9b2088f5bb4c812c9f";
    private final CountryRepo repo;

    @Autowired
    public OutputRepoImpl(CountryRepo repo) {
        this.repo = repo;
    }

    @Override
    public double getRespectiveCurrency(String currency, String startingCountry) {
        double exchangeRate;
        String result;
        try {
            URL url = new URL("https://free.currconv.com/api/v7/convert?q=" + currency +
                    "_" + repo.getCurrency(startingCountry).getCurrency() + "&compact=ultra&apiKey="
                    + API_KEY);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);
            result = jsonObject.get(currency + "_" + repo.getCurrency(startingCountry).getCurrency()).toString();

            exchangeRate = Double.parseDouble(result);
            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return exchangeRate;
    }
}
