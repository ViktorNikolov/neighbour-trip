package com.task.tour.repositories;

import com.task.tour.models.Country;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StartingCountryRepoImpl implements StartingCountryRepo {

    private final CountryRepo repo;

    @Autowired
    public StartingCountryRepoImpl(CountryRepo repo) {
        this.repo = repo;
    }

    @Override
    public String getFullName(String startingCountry) {
        String name;
        String result;
        try {
            URL url = new URL("https://restcountries.eu/rest/v2/alpha/"
                    + startingCountry);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);
            name = jsonObject.get("name").toString();

            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return name;
    }

    @Override
    public List<Country> getNeighbours(String startingCountry) {
        String result;
        List<Country> countries = new ArrayList<>();

        try {
            URL url = new URL("https://restcountries.eu/rest/v2/alpha/"
                    + startingCountry);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = (JSONArray) jsonObject.get("borders");

            for (int i = 0; i < jsonArray.length(); i++) {
                Country country = new Country();

                country.setName(jsonArray.get(i).toString());
                country.setCurrency(repo.getCurrency(jsonArray.get(i).toString()).getCurrency());
                country.setFullName(repo.getCurrency(jsonArray.get(i).toString()).getFullName());

                countries.add(i, country);
            }

            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return countries;
    }
}
