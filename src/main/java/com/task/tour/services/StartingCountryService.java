package com.task.tour.services;

import com.task.tour.models.Country;

import java.util.List;

public interface StartingCountryService {

    List<Country> getNeighbours(String startingCountry);

    String getFullName(String startingCountry);

}
