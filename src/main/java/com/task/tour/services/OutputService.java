package com.task.tour.services;

import com.task.tour.models.Country;

import java.util.List;
import java.util.Map;

public interface OutputService {

    List<Country> getNeighbours (String startingCountry);

    int timesOfTours(double budgetPerCountry, double totalBudget, String startingCountry);

    double getLeftOver(double budgetPerCountry, double totalBudget, String startingCountry);

    String getNeighbours(double budgetPerCountry, double totalBudget, String startingCountry);

    String getTimesOfTour(double budgetPerCountry, double totalBudget, String startingCountry);

    String getStringLeftOver(double budgetPerCountry, double totalBudget, String startingCountry, String currency);

    String getCurrency(double budgetPerCountry, double totalBudget, String startingCountry,
                       String currency);
}
