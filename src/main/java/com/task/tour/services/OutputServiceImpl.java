package com.task.tour.services;

import com.task.tour.models.Country;
import com.task.tour.repositories.OutputRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OutputServiceImpl implements OutputService{

    private final StartingCountryService startingCountryService;

    private final OutputRepo outputRepo;

    @Autowired
    public OutputServiceImpl(StartingCountryService startingCountryService, OutputRepo outputRepo) {
        this.startingCountryService = startingCountryService;
        this.outputRepo = outputRepo;
    }

    public List<Country> getNeighbours (String startingCountry) {
        List<Country> neighbours = startingCountryService.getNeighbours(startingCountry);

        return neighbours;
    }

    public int timesOfTours(double budgetPerCountry, double totalBudget, String startingCountry) {
        int times = (int) Math.floor(totalBudget / (budgetPerCountry * startingCountryService.
                getNeighbours(startingCountry).size()));

        return times;
    }

    @Override
    public double getLeftOver(double budgetPerCountry, double totalBudget, String startingCountry) {

        double leftOver = totalBudget - (timesOfTours(budgetPerCountry, totalBudget, startingCountry) *
                (budgetPerCountry * startingCountryService.
                        getNeighbours(startingCountry).size()));

        if (leftOver < 0)
            leftOver = 0;

        return leftOver;
    }

    @Override
    public String getNeighbours(double budgetPerCountry, double totalBudget, String startingCountry) {

        StringBuilder neighbourCountry = new StringBuilder();

        for (int i = 0; i < getNeighbours(startingCountry).size(); i++) {
            neighbourCountry.append(getNeighbours(startingCountry).get(i).getName());
            if (i == getNeighbours(startingCountry).size() - 1)
                break;

            neighbourCountry.append(", ");
        }

        String neighbours = startingCountryService.getFullName(startingCountry) + " has " + getNeighbours(startingCountry).size() +
                " neighbours countries (" + neighbourCountry.toString() + ") ";


        return neighbours;
    }

    @Override
    public String getTimesOfTour(double budgetPerCountry, double totalBudget, String startingCountry) {
        String timesOfTour = "and Angel can travel around them " + timesOfTours(budgetPerCountry,
                totalBudget, startingCountry) + " times.";

        return timesOfTour;
    }

    @Override
    public String getStringLeftOver(double budgetPerCountry, double totalBudget, String startingCountry,
                                    String currency) {
        String leftOVer = " He will have " + getLeftOver(budgetPerCountry, totalBudget, startingCountry) + " " +
                currency + " leftover. ";

        return leftOVer;
    }

    @Override
    public String getCurrency(double budgetPerCountry, double totalBudget, String startingCountry,
                              String currency) {
        StringBuilder respectiveCurrency = new StringBuilder();
        for (int i = 0; i < getNeighbours(startingCountry).size(); i++) {
            if (i == 0)
            respectiveCurrency.append("For ");
            else
            respectiveCurrency.append("for ");

            respectiveCurrency.append(getNeighbours(startingCountry).get(i).getFullName());
            respectiveCurrency.append(" he will need to buy ");
            double exchangeRate = outputRepo.getRespectiveCurrency(currency,
                    getNeighbours(startingCountry).get(i).getName()) * budgetPerCountry *
                    timesOfTours(budgetPerCountry, totalBudget, startingCountry);
            String rate = String.format("%.2f", exchangeRate);
            respectiveCurrency.append(rate);
            respectiveCurrency.append(getNeighbours(startingCountry).get(i).getCurrency());

            if (i == getNeighbours(startingCountry).size() - 1) {
                respectiveCurrency.append(".");
                break;
            }

            respectiveCurrency.append(", ");

        }
        return respectiveCurrency.toString();
    }
}
