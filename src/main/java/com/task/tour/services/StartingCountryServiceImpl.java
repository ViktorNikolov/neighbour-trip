package com.task.tour.services;

import com.task.tour.models.Country;
import com.task.tour.repositories.StartingCountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StartingCountryServiceImpl implements StartingCountryService {

    private final StartingCountryRepo repo;

    @Autowired
    public StartingCountryServiceImpl(StartingCountryRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<Country> getNeighbours(String startingCountry) {
        return repo.getNeighbours(startingCountry);
    }

    @Override
    public String getFullName(String startingCountry) {
        return repo.getFullName(startingCountry);
    }


}
