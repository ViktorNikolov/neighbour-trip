package com.task.tour.models;

import java.util.ArrayList;

public class StartingCountry{

    private String fullName;

    private ArrayList<Country> neighbours;

    public StartingCountry() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ArrayList<Country> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(ArrayList<Country> neighbours) {
        this.neighbours = neighbours;
    }
}
