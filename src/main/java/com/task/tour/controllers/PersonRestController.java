package com.task.tour.controllers;

import com.task.tour.services.OutputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PersonRestController {

    private final OutputService outputService;

    @Autowired
    public PersonRestController(OutputService outputService) {
        this.outputService = outputService;
    }

    @GetMapping
    public String getResult(@RequestParam double budgetPerCountry, @RequestParam double totalBudget,
                                       @RequestParam String startingCountry, @RequestParam String currency) {
        StringBuilder output = new StringBuilder();
        output.append(outputService.getNeighbours(budgetPerCountry, totalBudget, startingCountry));
        output.append(outputService.getTimesOfTour(budgetPerCountry, totalBudget, startingCountry));
        output.append(outputService.getStringLeftOver(budgetPerCountry, totalBudget, startingCountry, currency));
        output.append(outputService.getCurrency(budgetPerCountry, totalBudget, startingCountry, currency));

        return output.toString();
    }
}
